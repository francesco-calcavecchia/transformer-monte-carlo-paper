\documentclass{article}
\usepackage{latexsym,amsmath,amssymb,amsthm}
\usepackage{graphicx}
\usepackage{hyperref}

\newcommand{\invt}{{t^{-1}}}
\newcommand{\Omegap}{{\Omega}^{\prime}}
\newcommand{\detJ}{{\det(J_t)}}
\newcommand{\detJti}{{\det(J_{t_i})}}
\newcommand{\Rn}{{\mathbb{R}^n}}
\newcommand{\gt}{{g_t}}
\newcommand{\var}{{\text{var}}}
\newcommand{\Nmc}{{N_{\text{MC}}}}
\newcommand{\sign}{\text{sign}}

\title{Transformer Monte Carlo}
\author{Francesco Calcavecchia}

\begin{document}
  \maketitle

  \section{The mathematical justification}
  \label{sec:mathematical_justification}

  We are interested in the integral
  \begin{equation*}
    I = \int_{\Omega} dX \, f(X) \; .
  \end{equation*}

  Without any loss of generality, we decompose $f$ in the product of a probability densitity function (pdf) $\rho$ times a function $g$. Notice that it is always possible to do so, as one can define $\rho \equiv \frac{1}{V(\Omega)}$, where $V(\Omega)$ is the volume of the integration domain $\Omega$, and $g \equiv V(\Omega) \, f$. Let us then rewrite the integral as
  \begin{equation}
    I = \int_{\Omega} dX \, \rho(X) \, g(X) \; . \label{eq:original_I}
  \end{equation}

  We now introduce a bijection $t: \Omegap \rightarrow \Omega$, and its inverse $\invt: \Omega \rightarrow \Omegap$, and use them to re-write $I$ as
  \begin{eqnarray*}
    I & = & \int_{\Omega} dt(\invt(X)) \, \rho(t(\invt(X))) \, g(t(\invt(X)))   \\
      &   & \text{we define $Y \equiv \invt(X)$}   \\
      & = & \int_{\Omega} dt(Y) \, \rho(t(Y)) \, g(t(Y)) \\
      &   & \text{we change the variable of integration} \\
      & = & \int_{\Omegap} dY \, \rho(t(Y)) \, g(t(Y)) \, \detJ(Y)
  \end{eqnarray*}
  where $\detJ(Y)$ denotes the determinant of the Jacobian matrix of partial derivatives of $t$ at the point $Y$ (in one dimension it reduces to the simple first derivative of $t$, i.e. $\detJ(Y) \mapsto_{n=1} t^{\prime}(y)$). In the rest of this article we will refer to $t$ as coordinate \emph{transformation}.

  In the interesting case $\Omegap = \Omega$, we can write
  \begin{equation}
    I = \int_{\Omega} dX \, \rho(t(X)) \, g(t(X)) \, \detJ(X) \; . \label{eq:transformed_I}
  \end{equation}

  Combining eq. \ref{eq:original_I} and \ref{eq:transformed_I}, we are allowed to write
  \begin{eqnarray}
    I & = & \alpha_0 \int_{\Omega} dX \, \rho(X) \, g(X) + \alpha_1 \int_{\Omega} dX \, \rho(t(X)) \, g(t(X)) \, \detJ(X)  \nonumber \\
      & = & \int_{\Omega} dX \, \rho(X) \, \left[ \alpha_0 \, g(X) + \alpha_1 \, \frac{\rho(t(X))}{\rho(X)} \, g(t(X)) \, \detJ(X) \right] \; .   \label{eq:twin_I}
  \end{eqnarray}
  where $\alpha_0, \alpha_1 \in [0,1]$, and such that $\alpha_0 + \alpha_1 = 1$.
  This last equality suggests that we may integrate in a Monte Carlo fashion, by sampling from the pdf $\rho$, and averaging over the term in the square brackets.
  Compared to a regular Monte Carlo approach, we have the freedom to inject a transformation $t$ in the estimation, without introducing any bias. The effect of introducing $t$ is solely on the standard error associated to the estimation of $I$.

  We name this Monte Carlo integration method \emph{Transformer Monte Carlo} (TMC).
  Furthermore, we name \emph{transformed estimator} and denote as $\gt$ the term in eq. \ref{eq:twin_I}
  \begin{equation}
    \gt \equiv \frac{\rho(t(X))}{\rho(X)} \, g(t(X)) \, \detJ(X) \; .   \label{eq:twin_estimator}
  \end{equation}

  The method can be easily extended to multiple transformations:
  \begin{equation}
    I = \int_{\Omega} dX \, \rho(X) \left( \sum_{i=0}^{N_t} \alpha_i g_{t_i}(X) \right) \; ,   \label{eq:tmc_integral}
  \end{equation}
  where $t_0$ is the identity transformation, $\alpha_i \in [0, 1]$ for all $i$, and $\sum_{i=0}^{N_t} \alpha_i = 1$.

  For appropriate choices of $t_i$ and $\alpha_i$, one can reduce the Monte Carlo estimation variance.


  \section{A simple analytical example}
  \label{sec:simple_analytical_example}

  In this subsection, we would like to illustrate the TMC method with a simple analytical example. Let us consider the following one-dimensional integral:
  \begin{equation}
    I = \int_{-\infty}^{\infty} dx \, \frac{e^{-x^2}}{\sqrt{\pi}} \, \cos(20 \, x) \label{eq:analytical_example_integral} \; .
  \end{equation}

  We define
  \begin{eqnarray*}
    \rho &=& \frac{e^{-x^2}}{\sqrt{\pi}} \; , \\
    g    &=& \cos(20 \, x) \; .
  \end{eqnarray*}

  The associated Monte Carlo variance can be analytically computed:
  \begin{equation*}
    \var = \int_{-\infty}^{\infty} \, dx \, \rho(x) \, g^2(x) - \left( \int_{-\infty}^{\infty} \, dx \, \rho(x) \, g(x) \right)^2 = \frac{1}{2} \; .
  \end{equation*}

  Our goal is now to introduce transformations and weights that reduce this value, making the integration more efficient. We will present the choices as arbitrary, and leave the \href{https://gitlab.com/francesco-calcavecchia/deeptwinsamplingmontecarlo-notebooks/-/tree/master/2020-12-06-transformer_mcmc_with_analytically_known_transformation}{reference to our additional material} to the interested reader.

  \begin{description}
    \item [One transformation:] We choose
      \begin{eqnarray*}
        t_1(x)   &=& x + \frac{\pi}{20} \; , \\
        \alpha_0 &=& \frac{1}{2} \; , \\
        \alpha_1 &=& \frac{1}{2} \; . \\
      \end{eqnarray*}
      Then, $\var \approx 0.006$.
    \item [Two transformations:] We keep $t_1$ and choose
      \begin{eqnarray*}
        t_2(x)   &=& x - \frac{\pi}{20} \; , \\
        \alpha_0 &=& \frac{1}{2} \; , \\
        \alpha_1 &=& \frac{1}{4} \; , \\
        \alpha_1 &=& \frac{1}{4} \; .
      \end{eqnarray*}
      Then, $\var \approx 0.00015$.
  \end{description}

  With simple mathematical manipulations we have reduced the variance by a factor $\approx 10^2$ (one transformation) and $10^3$ (two transformations).
  The result is illustrated in Fig. \ref{fig:analytical_example_integrand_with_t1_and_t2}.

  \begin{figure}
    \includegraphics[width=\linewidth]{../images/analytical_example_integrand_with_t1_and_t2.png}
    \caption{Integrand of Eq. \ref{eq:analytical_example_integral}, in the original form, and after applying $t_1$ and $t_2$ as described in section \ref{sec:simple_analytical_example}. The transformations smooth the oscillations of the integrand, accelerating the convergence of the integral estimation to its correct value ($\approx \, 10^{-44}$).}
    \label{fig:analytical_example_integrand_with_t1_and_t2}
  \end{figure}



  \section{Integration domain inversion}
  \label{sec:integration_domain_generalization}

  Let us consider the simple case $t(X) = -X$. In this case the set $\Omegap$ is clearly identical to $\Omega$. However, this is not true for the corresponding open set used to integrate.

  For example, in the one dimensional case where $\Omega = \mathbb{R}$, Eq. \ref{eq:transformed_I} reads
  \begin{equation*}
  I = \int_{\invt(-\infty) = + \infty}^{\invt(+\infty) = -\infty} dx \, \rho(t(x)) \, g(t(x)) \, \detJ(X) \; .
  \end{equation*}
  If we want to combine it with the original integral, as we did to derive Eq. \ref{eq:twin_I}, we need to flip the integration boundaries, which will lead to a minus sign in front of the intgral.

  This entails that for such a transformation
  $$
  \int_{\Omegap} dx \, f(x) = - \int_{\Omega} dx \, f(x) \; .
  $$
  This implies that
  \begin{equation}
  \int_{\Omegap} dx \, \rho(x) = - \int_{\Omega} dx \, \rho(x) \; .   \label{eq:transformed_rho_normalization}
  \end{equation}

  In other words, the integral over $\Omegap$ is the same as the one over $\Omega$ only if we flip the integration sign.

  How can we generalize TMC to track this sign? This can be accomplished by tracking the sign of Eq. \ref{eq:transformed_rho_normalization}. In practice this simply means to modify Eq. \ref{eq:tmc_integral}:
  \begin{equation}
  I = \int_{\Omega} dX \, \rho(X) \left( \varcal{S}_{t_i} \, \sum_{i=0}^{N_t} \alpha_i g_{t_i}(X) \right) \; ,   \label{eq:generalized_tmc_integral}
  \end{equation}
  where
  $$
  \varcal{S}_{t_i} = \sign \left( \int_{\Omega} dX \, \rho(X) \, \frac{\rho(t_i(X))}{\rho(X)} \, \detJti(X) \right) \; .
  $$
  Notice that computing $\varcal{S}_{t_i}$ does not require any extra computational cost.

  Eq. \ref{eq:generalized_tmc_integral} can be considered as the most general TMC equation, which is able to accomodate transformations which lead to a sign flip. This is extremelly important, as such cases may artificially lead the variance of the integral to an exact zero. See \href{https://gitlab.com/francesco-calcavecchia/transformer-monte-carlo-notebooks/-/blob/master/2021-04-18-issue_with_integration_estimator_sign/1%20-%20Simple%20analytical%20cases.ipynb}{this sage notebook} for illustrative examples of the issue, and \href{https://gitlab.com/francesco-calcavecchia/transformer-monte-carlo-notebooks/-/blob/39b2c9ccc2065048d0992c92265cdc49a2e4524d/2022-01-03-fix_proposal_for_issue_with_integration_estimator_sign/1%20-%20compute%20sign%20of%20transformed%20volume.ipynb}{this one} to see that our proposed correction solves the issue.



\end{document}
